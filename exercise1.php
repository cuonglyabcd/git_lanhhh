<?php

$files = ['file1.txt', 'file2.txt'];
/**
 * @param array $read
 * @return int
 */
function countSentences($read)
{
    if (!empty($read)) {
        $read = implode(" ", $read);
        return substr_count($read, '.');      
    }
}

/**
 * @param array $read
 * @return bool
 */
function checkValidString($read)
{
    if (!empty($read)) {
        $read = implode(" ", $read);
        return (strpos($read, 'restaurant') XOR (strpos($read, 'book')));         
    }
    return false;
}

foreach ($files as $file) {
    $read = file($file);
    if (checkValidString($read)) {

        echo 'Chuỗi hợp lệ ';
        echo countSentences($read);
        echo ' câu' . '<br>';
    } else {
        echo 'Chuỗi không hợp lệ';
    }
}

?>
